#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
#
# -----
# HISTORY:
# Date      	By	Comments
# ----------	---	----------------------------------------------------------
###
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QTreeWidgetItem, QStyle
from matplotlib.backends.qt_compat import QtCore, QtWidgets, QtGui
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas, NavigationToolbar2QT as NavigationToolbar

import os
import sys
import re
import glob
import h5py
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QTreeWidgetItem, QStyle
from matplotlib.backends.qt_compat import QtCore, QtWidgets, QtGui
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas, NavigationToolbar2QT as NavigationToolbar

import warnings
warnings.filterwarnings('ignore')

from Ui_RFPulseViewer import Ui_QMainWindow

from RFPulseAnalyzer import * 

class MainRFViewer(QMainWindow,Ui_QMainWindow):
    def __init__(self,parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self._path=os.getcwd()
        self._data={}
        self._measure={}
        self.__printtree()
        self.__controls()
        self.__bindings()

    def __printtree(self):
        cmlist=sorted(glob.glob('CM*'))
        if cmlist is not None:
            for cm in cmlist:
                self._data[cm]={}
                cmpath=os.path.join(self._path,cm)
                for (path, dirs, files) in os.walk(cmpath):
                    if path == cmpath:
                        cavlist=sorted(dirs)
                for cav in cavlist:
                    self._data[cm][cav]={}
                    cavpath=os.path.join(self._path,cm,cav)
                    for (path, dirs, files) in os.walk(cavpath):
                        self._data[cm][cav]=sorted(files)
        else:
            sys.exit('__printtree: None CMlist!')
        # configure the treeWidget
        try:
            self.treeWidget.setColumnCount(2)
            self.treeWidget.setHeaderLabels(["File","Type"])
            self.treeWidget.setColumnWidth(0,200)
            treeItems=[]
            for k,v in self._data.items():
                CMItem=QTreeWidgetItem([k,'Folder'])
                CMItem.setIcon(0,QApplication.style().standardIcon(QStyle.SP_DirOpenIcon))
                CMItem.setBackground(0,QtGui.QBrush(QtGui.QColor(211,211,211)))
                CMItem.setBackground(1,QtGui.QBrush(QtGui.QColor(211,211,211)))
                for kk,vv in v.items():
                    CavItem=QTreeWidgetItem([kk,'Folder'])
                    CavItem.setIcon(0,QApplication.style().standardIcon(QStyle.SP_DirOpenIcon))
                    CavItem.setBackground(0,QtGui.QBrush(QtGui.QColor(224,255,255)))
                    CavItem.setBackground(1,QtGui.QBrush(QtGui.QColor(224,255,255)))
                    for vvv in vv:
                        ext=vvv.split(".")[-1]
                        child=QTreeWidgetItem([vvv,ext])
                        child.setToolTip(0,vvv)
                        child.setToolTip(1,vvv)
                        valid,items=self._isvalidHDFfile(k,kk,child)
                        if not valid:
                            child.setDisabled(1)
                            child.setIcon(0,QApplication.style().standardIcon(QStyle.SP_BrowserStop))
                        else:
                            child.setIcon(0,QtGui.QIcon(QtGui.QPixmap('ui/icons/HDF_logo.png')))
                            child.setBackground(0,QtGui.QBrush(QtGui.QColor(144,238,144)))
                            child.setBackground(1,QtGui.QBrush(QtGui.QColor(144,238,144)))
                            for item in items:
                                grp_child=QTreeWidgetItem([item[0],item[1].split(' ')[1]])
                                grp_child.setIcon(0,QtGui.QIcon(QtGui.QPixmap('ui/icons/dataset.png')))
                                child.addChild(grp_child)
                        CavItem.addChild(child)
                    CMItem.addChild(CavItem)
                treeItems.append(CMItem)
            self.treeWidget.addTopLevelItems(treeItems)
        except:
            print('__printtree: Morto!')

    def __controls(self):
        try:
            self.treeWidget.itemDoubleClicked.connect(self._onItemClicked)
        except:
            print('__controls: Morto!')

    def __bindings(self):
        try:
            self._string='{:s} root - /\nUser property file - {:s}'.format(os.path.basename(sys.argv[0]),self._path)
            self.lblMsg.setText(self._string)
        except:
            print('__bindings: Morto!')

    def _isvalidHDFfile(self,cm,cav,childitem):
        if childitem.text(1) not in {'hdf','hdf5'}:
            return False,None
        else:
            filepath=os.path.join(self._path,cm,cav,childitem.text(0))
            items=[]
            with h5py.File(filepath,'r') as f:
                if 'llrf' and 'srf' in f:
                    for grp in f.keys():
                        items.append([grp,str(f[grp])])
                    return True,items
                else:
                   return False,None

    def _getParentPath(self,item):
        def _getParent(item,outstring):
            if item.parent() is None:
                return outstring
            outstring = item.parent().text(0)+"/"+outstring
            return _getParent(item.parent(),outstring)
        output=_getParent(item,item.text(0))
        return output

    def _onItemClicked(self):
        item=self.treeWidget.currentItem()
        self.selItem=self._getParentPath(item)
        if self.selItem.endswith((".hdf",".hdf5")):
            self.lblMsg.setText(self._string+'/'+self.selItem)
            filepath=os.path.join(self._path,self.selItem)
            self._measure=LoadHDFFile(filepath)
            # call the function
            self._loadTbl()
            llrfTau, llrfQl=LLRFPlots(self._measure,self.plotllrfwidget.canvas)
            self.txtllrfTau.setText('{:.2f} us'.format(llrfTau))
            self.txtllrfQl.setText('{:.2e}'.format(llrfQl))           
            srfTau, srfQl=SRFPlots(self._measure,self.plotsrfwidget.canvas)
            self.txtsrfTau.setText('{:.2f} us'.format(srfTau))
            self.txtsrfQl.setText('{:.2e}'.format(srfQl))
            TunerPlot(self._measure,self.plottunerwidget.canvas)
            DetuningPlot(self._measure,self.plotdetuningwidget.canvas)
            #print(self.findChildren(QtWidgets.QSlider)[0].objectName())
            SlidePlots(self._measure,self.plotslidewidget.canvas,self.SliderTstart,self.SliderTend,self.txtSliderTstart,self.txtSliderTend,self.btnSliderReset) 
            PicoPlots(self._measure,self.plotpicoscopewidget.canvas)

    def _loadTbl(self):
        lstinfo=[]
        for k,v in self._measure.items():
            if not isinstance(v,(list,tuple,np.ndarray)):
                lstinfo.append({"name":k, "type":str(type(k)), "array size":'Scalar', "value":v})
        self.tableWidget.verticalHeader().setVisible(False)
        self.tableWidget.setColumnWidth(0,150)
        self.tableWidget.setColumnWidth(3,400)
        self.tableWidget.setRowCount(len(lstinfo))
        row=0
        for info in lstinfo:
            name_=QtWidgets.QTableWidgetItem(info["name"])
            type_=QtWidgets.QTableWidgetItem(info["type"])
            arr_=QtWidgets.QTableWidgetItem(info["array size"])
            val_=QtWidgets.QTableWidgetItem(info["value"] if isinstance(info["value"],str) else str(info["value"]))
            cell=[name_,type_,arr_,val_]
            for i in range(0,len(cell)):
                if (row%2)==0:
                    cell[i].setBackground(QtGui.QBrush(QtGui.QColor(182,208,226)))
                self.tableWidget.setItem(row,i,cell[i])
            row=row+1

if __name__ == '__main__':
    # Check whether there is already a running QApplication (e.g., if running from an IDE).
    qapp = QtWidgets.QApplication.instance()
    # create pyqt5 app
    if not qapp:
        qapp = QtWidgets.QApplication(sys.argv)
    # create the instance of application window
    app = MainRFViewer()
    app.show()
    app.activateWindow()
    app.raise_()
    sys.exit(qapp.exec())
