1. DONE: RFPulseSaver.py program should accept 1 argument:
    - argv[1] is the cryomodule name
    - Example: RFPulseSaver.py CM04

2. TODO (MW): For the 'new' file format we should save the LLRF both in power and in MV/m (add a new trace to the file, with a distinct name, in the same way as for the power meter where we store both the W and the converted). Each dataset should have the PU at the cavity in Watts for both the LLRF and the PM.
    - If the LLRF conversion is enabled store the one in MV/m and then calculate from KT the cavity power level and store.
    - If the conversion is not enabled the information is at the module port. Read the KT and ATT, apply the attenuation to save the cavity power and then transform power into gradient and save. Check that ATT and KT are good or default to ATT=0 KT=1.
    
3. TODO (MW): Save also the attenuation values in attributes (new format)

4. TODO (MW): RFPulseSaver should save the files in a folder structure, from the local directory. For the moment we specify the full path to RFPulseViewer, no need to look in the structure, it is an explicit argv with the full path.
    - First Directory level the CM (determined from argv)
    - Second Directory level the CAV (determined from OSP settings)
    - Example: CM04/CAV1 etc.

5. TODO (any): Add a functionality that the textual output and the plots of the saved data can be published into olog.

6. TODO (both): We should make more analysis to reconstruct the KT from the available data and do the same checks that are done in the IOC, so that we can again confirm we put the right values. We already tested 3 cryomodules where we have partial dataset. From CM04 on we should make sure that the dataset are complete.
