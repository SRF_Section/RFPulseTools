#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
#
# -----
# HISTORY:
# Date      	By	Comments
# ----------	---	----------------------------------------------------------
###
import os
import sys
import glob
import h5py
import scipy
import math
import numpy as np
import datetime

import warnings
warnings.filterwarnings('ignore')

MB = ['CM01','CM02','CM03','CM04','CM05','CM06','CM07','CM08','CM09']

props=dict(facecolor='None',alpha=0.5,edgecolor='None')
textkwargs=dict(fontsize=10,backgroundcolor='w',ha='center',va='center')
legendkwargs=dict(loc=0,fontsize=8,shadow=True)

def LoadHDFFile(filename):
    measure={}
    measure['Filename']=os.path.basename(filename)
    with h5py.File(filename,'r') as f:
        for k,v in f.attrs.items():
            measure[k]=v
        if not 'OSP230' in measure:  measure['OSP230']=0
        if not 'Version' in measure: measure['Version']='- N/A -'
        for key,grp in f.items():
            if isinstance(grp,h5py.Group):
                get_Traces(measure,f[key])
        if not 'OpenLoop' in measure: measure['OpenLoop']=1
        if 'SP_Fill' in measure: measure['SP_Fill[ms]']=measure.pop('SP_Fill')
        if 'SP_Tau' in measure: measure['SP_Tau[ms]']=measure.pop('SP_Tau')
        if 'SP_Field' in measure: measure['SP_Field[MV/m]']=measure.pop('SP_Field')
    return measure

def get_Traces(measure,grp):
    for k_attr,v_attr in grp.attrs.items():
        measure[k_attr]=v_attr
    for k,v in grp.items():
        if isinstance(v,h5py.Dataset):
            measure[k]=v[()]
            for kk,vv in v.attrs.items():
                if kk.startswith('AI') or kk.startswith('Ch'):
                    measure['{:s}-Desc'.format(k)]=v.attrs[kk]
    try:
        units=v.attrs['Unit'].split(',')
        measure['{:s}-Units'.format(k)]=units
    except:
        try:
            units=v.attrs['Units'].split(',')
            measure['{:s}-Units'.format(k)]=units
        except:
            pass

def getStatistics(tstart,tend,trace):
    flattop=trace[1][(trace[0]>=tstart)&(trace[0]<=tend)]
    mean=np.mean(flattop)
    std=np.std(flattop)
    return mean,std

def get_Ql(plength,fillvalue,cavpu,min,max):
    decay_cond=(cavpu[0]>=plength)&(cavpu[1]<=max*fillvalue)&(cavpu[1]>=min*fillvalue)
    decay=np.array([cavpu[0][decay_cond],cavpu[1][decay_cond]])
    decaylog=np.array([decay[0],np.log(decay[1])])
    new=np.polynomial.polynomial.Polynomial.fit(decaylog[0]-plength,decaylog[1],1)  
    cof=new.convert().coef
    tau=-1./cof[1]*1000
    Ql=np.pi*704.420*tau
    return decay,tau,Ql

def LLRFPlots(meas,canvas):
    if meas['OSP230'] == 0:
        pulsewidth = float(meas['RF Pulse Width[us]'])/1000
        reprate    = float(meas['Repetition Rate[Hz]'])
        grad       = float(meas['Cavity Gradient[MV]'])
        filltime   = float(meas['FF_FillingTime[ms]'])
        pwrsp      = float(meas['FF_PowerP[kw]'])
        fillratio  = float(meas['FF_FillRatio'])
    else:
        pulsewidth = meas['RF Pulse Width[us]']/1000
        reprate    = meas['Repetition Rate[Hz]']
        grad       = meas['Cavity Gradient[MV]']
        filltime   = meas['FF_FillingTime[ms]']
        pwrsp      = meas['FF_PowerP[kw]']
        fillratio  = meas['FF_FillRatio']
    # MB/HB cavity parameters
    if meas['CryoModule'] in MB:
        Leff   = 0.855
        RoverQ = 374
    else:
        Leff   = 0.915
        RoverQ = 435
    # CL status
    if not meas['OpenLoop']:
        filltime = meas['SP_Fill[ms]'] if isinstance(meas['SP_Fill[ms]'],float) else float(meas['SP_Fill[ms]'])
        sp_tau   = meas['SP_Tau[ms]'] if isinstance(meas['SP_Tau[ms]'],float) else float(meas['SP_Tau[ms]'])
        sp_eacc  = meas['SP_Field[MV/m]'] if isinstance(meas['SP_Field[MV/m]'],float) else float(meas['SP_Field[MV/m]'])
        fillratio= pow(1/(1-math.exp(-filltime/sp_tau)),2)

    if fillratio>1:
        start=filltime
        end=pulsewidth
    else:
        if pulsewidth<1.5:
            print('*** PULSE TOO SHORT ***\nResult may be funny...')
        start=pulsewidth-0.5
        end=pulsewidth
    # llrf data
    llrfpu  = meas['LLRFPTR']
    llrfkly = meas['LLRFFWD']
    llrffwd = meas['LLRFCFW']
    llrfref = meas['LLRFCREF']
    # Statistics for mean and std
    mean,std      = getStatistics(start,end,llrfpu)
    meankly,stdkly= getStatistics(start,end,llrfkly)
    meancf,stdcf  = getStatistics(start,end,llrffwd)
    meancr,stdcr  = getStatistics(start,end,llrfref)
    # Decay time and QL
    # TODO: Read and store the LLRF Gradient calibration conversion and use it in the new file format
    if 'LLRFPTRW' not in meas.keys():
        units=meas['LLRFPTR-Units']
        if np.max(llrfpu[1])>2.0:
            print('Old file format, data in MV/m')
            units[1]='MV/m'
        else:
            print('Old file format, converting to MV/m')
            units[1]='MV/m'
            kteff=grad/np.sqrt(mean)
            llrfpu[1][llrfpu[1]<0]=0
            llrfpu[1]=kteff*np.sqrt(llrfpu[1])      
        yf=llrfpu[1][np.where(llrfpu[0]>=start)[0][0]]
        ye=llrfpu[1][np.where(llrfpu[0]>=end)[0][0]]
        decay,tau,Ql=get_Ql(pulsewidth,ye,llrfpu,0.5,0.9)
        # if we rescaled the llrfpu we need to recalculate the new mean
        mean,std=getStatistics(start,end,llrfpu)
    else:
        llrfpuw=meas['LLRFPTRW']
        kt=meas['kt']
        if 'V.2.0' in meas['Version']:
            print('Here the frigging messy version, trying to correct...')
            print('Recovered kt={:.2f}'.format(kt))
            llrfpu[1]=kt*np.sqrt(llrfpu[1])
            mean,std=getStatistics(start,end,llrfpu)
        units=meas['LLRFPTRW-Units']
        print('New file format, data in MV/m')
        units[1]='MV/m'
        yf=llrfpu[1][np.where(llrfpu[0]>=start)[0][0]]
        ye=llrfpu[1][np.where(llrfpu[0]>=end)[0][0]]
        decay,tau,Ql=get_Ql(pulsewidth,ye,llrfpu,0.5,0.9)
    # https://olog-ts2.esss.lu.se/logs/4508 (LLRF IOC updated: use degrees in phase channels)
    date=datetime.datetime.strptime(meas['Datetime'],"%Y-%m-%d %H:%M:%S")
    if date>datetime.datetime(2023,2,21,13,57,34): units[2]='Degree'
    # Start the plot
    canvas.fig.clear()
    ax=canvas.fig.subplots(2,2,sharex=True)
    # 0,0 is the cavity field amplitude
    ax[0,0].plot(decay[0],decay[1],'c',lw=4.0)
    ax[0,0].plot(llrfpu[0],llrfpu[1],'b',label='PU Cav')
    ax[0,0].plot([start,start],[0,yf],'k:')
    ax[0,0].plot([end,end],[0,ye],'k:')
    ax[0,0].plot([start,end],[mean,mean],'r')
    ax[0,0].plot([start,end],[mean-std,mean-std],'r:')
    ax[0,0].plot([start,end],[mean+std,mean+std],'r:')
    ax[0,0].set_ylabel(units[1])
    ax[0,0].legend(**legendkwargs)
    ax[0,0].set_title('CAV PU Amplitude')
    efwd=1/Leff*np.sqrt(4.0*RoverQ*Ql*meancf*1000)/1E6
    erev=1/Leff*np.sqrt(4.0*RoverQ*Ql*meancr*1000)/1E6
    ax[0,0].text((filltime+pulsewidth)*0.5 if pulsewidth>=1.5 else pulsewidth*2, mean*0.5,
        '''
            <{:.2f}> +- {:.2f} (rms {:.1f}%) {:s}
            From $P_{{fwd}}$ predicted {:.2f} MV/m
            From $P_{{rev}}$ predicted {:.2f} MV/m
        '''.format(mean,std,std/mean*100,units[1],efwd,erev),**textkwargs,bbox=props)
    # 0,1 is the cavity field phase
    ax[0,1].plot(llrfpu[0],llrfpu[2],'b',label='Phase Cav')
    ax[0,1].set_ylabel(units[2])
    ax[0,1].legend(**legendkwargs)
    ax[0,1].set_title('CAV PU Phase')
    # 1,0 is the forward/reflect traces
    units[1]='kW'
    ax[1,0].set_xlabel(units[0])
    ax[1,0].set_ylabel(units[1])
    ax[1,0].plot(llrffwd[0],llrffwd[1],'r',label='FWD Cav')
    ax[1,0].plot(llrfkly[0],llrfkly[1],'k',label='FWD Kly')
    ax[1,0].plot(llrfref[0],llrfref[1],'g',label='REV Cav')
    ax[1,0].legend(**legendkwargs)
    ax[1,0].set_title('FWD/REV Power')
    pt=max(meancf,meancr,meankly)+3*max(stdcf,stdcr,stdkly)
    ax[1,0].text((filltime+pulsewidth)*0.5 if pulsewidth>=1.5 else pulsewidth*2, pt*1.5 if pulsewidth>=1.5 else pt*0.5,
        '''
            {:s} Setpoint {:.2f} {:s}
            $P_{{fwd}}$ <{:.2f}> +- {:.2f} {:s}
            $P_{{rev}}$ <{:.2f}> +- {:.2f} {:s}
            $P_{{kly}}$ <{:.2f}> +- {:.2f} {:s}
        '''.format('FF' if meas['OpenLoop'] else 'SP',pwrsp if meas['OpenLoop'] else sp_eacc, 'kW' if meas['OpenLoop'] else 'MV/m',
                meancf,stdcf,units[1],meancr,stdcr,units[1],meankly,stdkly,units[1]),**textkwargs,bbox=props)
    if not meas['OpenLoop']:
        print('\n\tFilling power: {:.2f}kW Flattop power: {:.2f}kW'.format(pow(sp_eacc*Leff,2)/(4*RoverQ*Ql)*1e9,pow(sp_eacc*Leff,2)/(4*RoverQ*Ql)*1e9*fillratio))
    # 1,1 is the forward/reflect phases
    ax[1,1].plot(llrffwd[0],llrffwd[2],'r',label='Pha FWD Cav')
    ax[1,1].plot(llrfkly[0],llrfkly[2],'k',label='Pha FWD Kly')
    ax[1,1].plot(llrfref[0],llrfref[2],'g',label='Pha REV Cav')
    ax[1,1].set_xlabel(units[0])
    ax[1,1].set_ylabel(units[2])
    ax[1,1].legend(**legendkwargs)
    ax[1,1].set_title('FWD/REV Phase')
    canvas.fig.tight_layout()
    canvas.draw()
    return tau,Ql

def SRFPlots(meas,canvas):
    if meas['OSP230']==0:
        pulsewidth = float(meas["RF Pulse Width[us]"])/1000
        reprate    = float(meas["Repetition Rate[Hz]"])
        filltime   = float(meas['FF_FillingTime[ms]'])
        pwrsp      = float(meas['FF_PowerP[kw]'])
        grad       = float(meas['Cavity Gradient[MV]'])
        kt         = float(meas['kt'])
        fillratio  = float(meas['FF_FillRatio'])
    else:
        pulsewidth = meas["RF Pulse Width[us]"]/1000
        reprate    = meas["Repetition Rate[Hz]"]
        filltime   = meas['FF_FillingTime[ms]']
        pwrsp      = meas['FF_PowerP[kw]']
        grad       = meas['Cavity Gradient[MV]']
        kt         = meas['kt']
        fillratio  = meas['FF_FillRatio']
    # MB/HB cavity parameters
    if meas['CryoModule'] in MB:
        Leff   = 0.855
        RoverQ = 374
    else:
        Leff   = 0.915
        RoverQ = 435
    # CL status
    if not meas['OpenLoop']:
        filltime = meas['SP_Fill[ms]'] if isinstance(meas['SP_Fill[ms]'],float) else float(meas['SP_Fill[ms]'])
        sp_tau   = meas['SP_Tau[ms]'] if isinstance(meas['SP_Tau[ms]'],float) else float(meas['SP_Tau[ms]'])
        sp_eacc  = meas['SP_Field[MV/m]'] if isinstance(meas['SP_Field[MV/m]'],float) else float(meas['SP_Field[MV/m]'])
        fillratio= pow(1/(1-math.exp(-filltime/sp_tau)),2)

    if fillratio>1:
        start=filltime
        end=pulsewidth
    else:
        if pulsewidth<1.5:
            print('*** PULSE TOO SHORT ***\nResult may be funny...')
        start=pulsewidth-0.5
        end=pulsewidth
    # srf data
    srfpu      = np.array([meas['RFPMPUW'][0]*1000,meas['RFPMPUW'][1]])
    srfeacc    = np.array([meas['RFPMPUW'][0]*1000,kt*np.sqrt(np.abs(meas['RFPMPUW'][1]))])
    srffwd     = np.array([meas['RFPMDCC'][0]*1000,meas['RFPMDCC'][1]])
    srfref     = np.array([meas['RFPMDCR'][0]*1000,meas['RFPMDCR'][1]])
    # Statistics
    mean,std     = getStatistics(start,end,srfeacc)
    meancf,stdcf = getStatistics(start,end,srffwd)
    meancr,stdcr = getStatistics(start,end,srfref)
    if meas['OSP230'] == 0:
        srfkly = np.array([meas['RFPMDCF'][0]*1000,meas['RFPMDCF'][1]])
        meankly,stdkly = getStatistics(start,end,srfkly)
    # Decay time and QL
    yf=srfeacc[1][np.where(srfeacc[0]>=start)[0][0]]
    ye=srfeacc[1][np.where(srfeacc[0]>=end)[0][0]]
    decay,tau,Ql=get_Ql(pulsewidth,ye,srfeacc,0.5,0.9)
    # Start the plot
    canvas.fig.clear()
    ax=canvas.fig.subplots(2,2)
    # 0,0 is the Cavity Field Amplitude [MV/m]
    ax[0,0].plot(decay[0],decay[1],'c',lw=4.0)
    ax[0,0].plot(srfeacc[0],srfeacc[1],'b',label='PU Cav')
    ax[0,0].plot([start,start],[0,yf],'k:')
    ax[0,0].plot([end,end],[0,ye],'k:')
    ax[0,0].plot([start,end],[mean,mean],'r')
    ax[0,0].plot([start,end],[mean-std,mean-std],'r:')
    ax[0,0].plot([start,end],[mean+std,mean+std],'r:')
    ax[0,0].set_ylabel('MV/m')
    ax[0,0].legend(**legendkwargs)
    ax[0,0].set_title('CAV PU Amplitude')
    efwd=1/Leff*np.sqrt(4.0*RoverQ*Ql*meancf*1000)/1E6
    erev=1/Leff*np.sqrt(4.0*RoverQ*Ql*meancr*1000)/1E6
    ax[0,0].text((filltime+pulsewidth)*0.5 if pulsewidth>=1.5 else pulsewidth*2 ,mean*0.5,
        '''
            <{:.2f}> +- {:.2f} (rms {:.1f}%) MV/m
            From $P_{{fwd}}$ predicted {:.2f} MV/m
            From $P_{{rev}}$ predicted {:.2f} MV/m
        '''.format(mean,std,std/mean*100,efwd,erev),**textkwargs,bbox=props)
    # 0,1 is the power in Watts comparison
    meanpu1,std1=getStatistics(start,end,srfpu)
    ax[0,1].plot(srfpu[0],srfpu[1],'b',label='SRF')
    if 'LLRFPTRW' not in meas.keys():
        llrfpuw=meas['LLRFPTR']
        if np.max(llrfpuw[1])>2.0:
            llrfpuw[1]=pow(llrfpuw[1]/kt,2)
    else:
        llrfpuw=meas['LLRFPTRW']
    meanpu2,std2=getStatistics(start,end,llrfpuw)
    ax[0,1].plot(llrfpuw[0],llrfpuw[1],'g',label='LLRF')
    ax[0,1].text((filltime+pulsewidth)*0.5 if pulsewidth>=1.5 else pulsewidth*2, meanpu1*0.5,
        '''
            SRF  <{:.2f}> +- {:.2f} W (rms {:.1f}%) 
            LLRF <{:.2f}> +- {:.2f} W (rms {:.1f}%)
            Difference {:.1f}%
        '''.format(meanpu1,std1,std1/meanpu1*100,meanpu2,std2,std2/meanpu2*100,np.abs(meanpu1-meanpu2)/((meanpu1+meanpu2)/2)*100),**textkwargs,bbox=props)
    ax[0,1].set_xlabel('ms')
    ax[0,1].set_ylabel('W')
    ax[0,1].legend(**legendkwargs)
    ax[0,1].set_title('PU Power Comparison')
    # 1,0 is the forward/reflect traces
    ax[1,0].plot(srffwd[0],srffwd[1],'r',label='FWD Cav')
    ax[1,0].plot(srfref[0],srfref[1],'g',label='REV Cav')
    if meas['OSP230']==0:
        ax[1,0].plot(srfkly[0],srfkly[1],'k',label='FWD Kly')
    ax[1,0].set_xlabel('ms')
    ax[1,0].set_ylabel('kW')
    ax[1,0].legend(**legendkwargs)
    ax[1,0].set_title('FWD/REV Power')
    if meas['OSP230']==0:
        pt=max(meancf,meancr,meankly)+3*max(stdcf,stdcr,stdkly)
        ax[1,0].text((filltime+pulsewidth)*0.5 if pulsewidth>=1.5 else pulsewidth*2, pt*1.5 if pulsewidth>=1.5 else pt*0.5,
            '''
                {:s} Setpoint {:.2f} {:s}
                $P_{{fwd}}$ <{:.2f}> +- {:.2f} {:s}
                $P_{{rev}}$ <{:.2f}> +- {:.2f} {:s}
                $P_{{kly}}$ <{:.2f}> +- {:.2f} {:s}
            '''.format('FF' if meas['OpenLoop'] else 'SP',pwrsp if meas['OpenLoop'] else sp_eacc, 'kW' if meas['OpenLoop'] else 'MV/m',
                    meancf,stdcf,'kW',meancr,stdcr,'kW',meankly,stdkly,'kW'),**textkwargs,bbox=props)
    else:
        pt=max(meancf,meancr)+3*max(stdcf,stdcr)
        ax[1,0].text((filltime+pulsewidth)*0.5 if pulsewidth>=1.5 else pulsewidth*2, pt*1.5 if pulsewidth>=1.5 else pt*0.5,
            '''
                {:s} Setpoint {:.2f} {:s}
                $P_{{fwd}}$ <{:.2f}> +- {:.2f} {:s}
                $P_{{rev}}$ <{:.2f}> +- {:.2f} {:s}
            '''.format('FF' if meas['OpenLoop'] else 'SP',pwrsp if meas['OpenLoop'] else sp_eacc, 'kW' if meas['OpenLoop'] else 'MV/m',
                    meancf,stdcf,'kW',meancr,stdcr,'kW'),**textkwargs,bbox=props)
    ax[1,1].text(0.5,0.5,'- N/A -',ha='center',va='center',fontsize='10')
    canvas.fig.tight_layout()
    canvas.draw()
    return tau,Ql

def TunerPlot(meas,canvas):
    canvas.fig.clear()
    ax=canvas.fig.subplots(1,1)
    if 'TUNER' in meas:
        tuning=np.array([meas['TUNER'][0],meas['TUNER'][1]-meas['TUNER'][1][0]])
        if tuning is not None and tuning[0].size==tuning[1].size and tuning[0].size>1:
            ax.plot(tuning[0],tuning[1],'b.:')
            model=np.polyfit(tuning[0],tuning[1],1)
            sens=model[0]
            ax.text(np.mean(tuning[0])*0.5,np.mean(tuning[1]),
                '''
                    Fitting:
                    {:.2f} kHz/Screw turn
                    {:.2f} Hz/step
                '''.format(sens*20000,sens*1000),**textkwargs,bbox=props)
            ax.set_title('Cavity Sensitivity')
            ax.set_xlabel('{:s}'.format(meas['TUNER-Units'][0]))
            ax.set_ylabel('{:s}'.format(meas['TUNER-Units'][1]))
        else:
            ax.text(0.5,0.5,'- Incomplete Data -',ha='center',va='center',fontsize='8')
    else:
        ax.text(0.5,0.5,'- N/A -',ha='center',va='center',fontsize='8')
    canvas.fig.tight_layout()
    canvas.draw()

def DetuningPlot(meas,canvas):
    if meas['OSP230'] == 0:
        pulsewidth = float(meas['RF Pulse Width[us]'])/1000
        grad       = float(meas['Cavity Gradient[MV]'])
        filltime   = float(meas['FF_FillingTime[ms]'])
    else:
        pulsewidth = meas['RF Pulse Width[us]']/1000
        grad       = meas['Cavity Gradient[MV]']
        filltime   = meas['FF_FillingTime[ms]']
    if not meas['OpenLoop']: filltime = meas['SP_Fill[ms]'] if isinstance(meas['SP_Fill[ms]'],float) else float(meas['SP_Fill[ms]'])
    # start the plot
    canvas.fig.clear()
    ax=canvas.fig.subplots(1,1)
    if 'DETUNING' in meas:
        detuning=meas['DETUNING']
        if detuning is not None:
            margin=0.025
            x=detuning[0][(detuning[0]>filltime+margin)&(detuning[0]<pulsewidth-margin)]
            y=detuning[1][(detuning[0]>filltime+margin)&(detuning[0]<pulsewidth-margin)]
            ax.plot(x,y,'b',label='detuning')
            ax.set_xlabel('ms')
            ax.set_ylabel('{:s}'.format(meas['DETUNING-Units'][1]))
            res=scipy.stats.linregress(x,y)
            if res.rvalue**2>=0.5:
                y_fit=res.intercept+res.slope*x
                ax.plot(x,y_fit,'r',label='fitted line')
                lfd=(y_fit[-1]-y_fit[0])/(grad**2)
                ax.text(np.mean(x)*0.5,np.mean(y_fit),
                    '''
                        Fitting correlation {:.2f}%
                        LFD coefficient K$_L$ {:.2f} [Hz/(MV/m)$^2$]
                    '''.format(lfd,res.rvalue**2*100),**textkwargs,bbox=props)
            ax.set_title('Cavity Detuning')
            ax.legend(**legendkwargs)
        else:
            ax.text(0.5,0.5,'- Incomplete Data -',ha='center',va='center',fontsize='8')
    else:
        ax.text(0.5,0.5,'- N/A -',ha='center',va='center',fontsize='8')
    canvas.fig.tight_layout()
    canvas.draw()

def SlidePlots(meas,canvas,sliderTstart,sliderTend,txtTstart,txtTend,btnSliderReset):
    def slide_llrf_plot(axs,ax2,meas,Tstart,Tend):
        llrfpu  = meas['LLRFPTR']
        llrffwd = meas['LLRFCFW']
        llrfref = meas['LLRFCREF']
        dt	= llrfpu[0][1]-llrfpu[0][0]
        i_start = int(Tstart/dt)
        i_end   = int(Tend/dt)
        mean,std = getStatistics(Tstart,Tend,llrfpu)
        axs[0,0].cla()
        axs[0,0].set_ylabel('MV/m')
        axs[0,0].ticklabel_format(useOffset=False)
        axs[0,0].plot(llrfpu[0][i_start:i_end],llrfpu[1][i_start:i_end],'b',label='$E_{acc}$')
        axs[0,0].legend(**legendkwargs)
        axs[0,0].set_title('Cavity PU Amplitude LLRF')
        axs[1,0].cla()
        axs[1,0].set_xlabel('ms')
        axs[1,0].set_ylabel('kW')
        axs[1,0].tick_params(axis='y',colors='r')
        ax2.cla()
        ax2.tick_params(axis='y',colors='g')
        lns1=axs[1,0].plot(llrffwd[0][i_start:i_end],llrffwd[1][i_start:i_end],'r',label='$P_{for}$')
        lns2=ax2.plot(llrfref[0][i_start:i_end],llrfref[1][i_start:i_end],'g',label='$P_{ref}$')
        lns=lns1+lns2
        labs = [l.get_label() for l in lns]
        axs[1,0].legend(lns,labs,**legendkwargs)
        axs[1,0].set_title('FWD/REV Power LLRF')

    def slide_srf_plot(axs,ax2,meas,Tstart,Tend):
        if meas['OSP230']==0:
            kt = float(meas['kt'])
        else:
            kt = meas['kt']
        srfpu   = meas['RFPMPUW']
        srfeacc = np.array([srfpu[0],kt*np.sqrt(np.abs(srfpu[1]))])
        srffwd  = meas['RFPMDCC']
        srfref  = meas['RFPMDCR']
        dt      = 1e6*(srffwd[0][-1]-srffwd[0][0])/len(srffwd[0])
        i_offset= int(srffwd[0][0]*1e6/dt)
        i_start = int(Tstart*1000/dt)-i_offset
        i_end   = int(Tend*1000/dt)-i_offset
        mean,std = getStatistics(Tstart/1000,Tend/1000,srfeacc)
        axs[0,1].cla()
        axs[0,1].ticklabel_format(useOffset=False)
        axs[0,1].plot(srfeacc[0][i_start:i_end]*1000,srfeacc[1][i_start:i_end],'b',label='$E_{acc}$')
        axs[0,1].legend(**legendkwargs)
        axs[0,1].set_title('Cavity PU Amplitude SRF')
        axs[1,1].cla()
        axs[1,1].set_xlabel('ms')
        axs[1,1].tick_params(axis='y',colors='r') 
        ax2.cla()
        ax2.tick_params(axis='y',colors='g')
        lns1=axs[1,1].plot(srffwd[0][i_start:i_end]*1000,srffwd[1][i_start:i_end],'r',label='$P_{for}$')
        lns2=ax2.plot(srfref[0][i_start:i_end]*1000,srfref[1][i_start:i_end],'g',label='$P_{ref}$')
        lns=lns1+lns2
        labs = [l.get_label() for l in lns]
        axs[1,1].legend(lns,labs,**legendkwargs)
        axs[1,1].set_title('FWD/REV Power SRF')

    # The function can be called at anytime when one of sliders value change
    def update():
        canvas.fig.clear()
        ax=canvas.fig.subplots(2,2,sharex=True)
        Tstart = sliderTstart.value()/10
        Tend   = sliderTend.value()/10
        if Tend<Tstart or (Tstart==0 and Tend==0):
            for axes in ax.ravel():
                axes.cla()
                axes.text(0.5,0.5,'- N/A -',ha='center',va='center',fontsize='8')
        else:
            # create the twinx() for axes
            twinax_llrf = ax[1,0].twinx()
            twinax_srf  = ax[1,1].twinx()
            slide_llrf_plot(ax,twinax_llrf,meas,Tstart,Tend)
            slide_srf_plot(ax,twinax_srf,meas,Tstart,Tend)
        canvas.fig.tight_layout()        
        canvas.draw()
        txtTstart.setText('{:.2f} ms'.format(Tstart))
        txtTend.setText('{:.2f} ms'.format(Tend))

    # slider widgets set to default values
    def reset():
        sliderTstart.valueChanged.disconnect()
        sliderTend.valueChanged.disconnect()
        sliderTstart.setValue(0)
        sliderTend.setValue(0)
        update()
        sliderTstart.valueChanged.connect(update)
        sliderTend.valueChanged.connect(update)

    # signals to slots
    sliderTstart.valueChanged.connect(update)
    sliderTend.valueChanged.connect(update)
    btnSliderReset.clicked.connect(reset)

    # call the function
    update()

def PicoPlots(meas,canvas):
    if not all(k in meas.keys() for k in ['PICOADAIR','PICOADVAC','PICOEPU']):
        canvas.fig.clear()
        axs=canvas.fig.subplots(1,1)
        axs.text(0.5,0.5,'- N/A -',ha='center',va='center',fontsize='8')
        return
    if meas['OSP230']==0:
        pulsewidth = float(meas['RF Pulse Width[us]'])
    else:
        pulsewidth = meas['RF Pulse Width[us]']
    # llrf and picoscope data
    adair   = meas['PICOADAIR']
    advac   = meas['PICOADVAC']
    epu     = meas['PICOEPU']
    llrfref = meas['LLRFCREF']
    # find peaks for AD/EPU trace
    thre_ad = 1.0; thre_epu = 0.35
    peaks_adair, _ = scipy.signal.find_peaks(adair[1][adair[0]>=pulsewidth], height=thre_ad, distance=100)
    peaks_advac, _ = scipy.signal.find_peaks(advac[1][advac[0]>=pulsewidth], height=thre_ad, distance=100)
    peaks_epu,   _ = scipy.signal.find_peaks(epu[1][epu[0]>=pulsewidth], height=thre_epu, distance=50)   
    # start the plot
    canvas.fig.clear()
    axs=canvas.fig.subplots(2,1,sharex=True)
    # AD plot
    axs[0].cla()
    lns1=axs[0].plot(llrfref[0],llrfref[1],'b',label='$P_{ref}$')
    ax2=axs[0].twinx()
    lns2=ax2.plot(adair[0]/1000,adair[1],'r',label='AD AIR')
    lns3=ax2.plot(advac[0]/1000,advac[1],'g',label='AD VAC')
    if any(peaks_adair):
        ax2.plot(adair[0]/1000,np.full_like(adair[1],thre_ad),"--",c='gray')
        offset = np.where(adair[0]>=pulsewidth)[0][0]
        for p in peaks_adair:
            idx2 = p+offset
            idx1 = np.where(llrfref[0]>=adair[0][idx2]/1000)[0][0]
            ax2.plot(adair[0][idx2]/1000,adair[1][idx2],'k*')
            axs[0].plot(llrfref[0][idx1],llrfref[1][idx1],'k*')
            ax2.plot(np.full_like(adair[0],adair[0][idx2]/1000),adair[1],'k-.')
            ax2.text(adair[0][idx2]/1000,adair[1][idx2]*0.8,'{:.2f}kW\n{:.2f}V\n{:.2f}ms'.format(llrfref[1][idx1],adair[1][idx2],llrfref[0][idx1]),**textkwargs)
    if any(peaks_advac):
        ax2.plot(advac[0]/1000,np.full_like(advac[1],thre_ad),"--",c='gray')
        offset = np.where(advac[0]>=pulsewidth)[0][0]
        for p in peaks_advac:
            idx2 = p+offset
            idx1 = np.where(llrfref[0]>=advac[0][idx2]/1000)[0][0]
            ax2.plot(advac[0][idx2]/1000,advac[1][idx2],'k*')
            axs[0].plot(llrfref[0][idx1],llrfref[1][idx1],'k*')
            ax2.plot(np.full_like(advac[0],advac[0][idx2]/1000),advac[1],'k-.')
            ax2.text(advac[0][idx2]/1000,advac[1][idx2]*0.8,'{:.2f}kW\n{:.2f}V\n{:.2f}ms'.format(llrfref[1][idx1],advac[1][idx2],llrfref[0][idx1]),**textkwargs)
    axs[0].set_ylabel('kW',rotation=0,labelpad=20)
    ax2.set_ylabel('V',rotation=0,labelpad=20)
    lns=lns1+lns2+lns3
    axs[0].legend(lns,[l.get_label() for l in lns],**legendkwargs)
    # EPU plot
    axs[1].cla()
    lns1=axs[1].plot(llrfref[0],llrfref[1],'b',label='$P_{ref}$')
    ax2=axs[1].twinx()
    lns2=ax2.plot(epu[0]/1000,epu[1],'r',label='EPU')
    if any(peaks_epu):
        ax2.plot(epu[0]/1000,np.full_like(epu[1],thre_epu),"--",c='gray')
        offset = np.where(epu[0]>=pulsewidth)[0][0]
        for p in peaks_epu:
            idx2 = p+offset
            idx1 = np.where(llrfref[0]>=epu[0][idx2]/1000)[0][0]
            ax2.plot(epu[0][idx2]/1000,epu[1][idx2],'k*')
            axs[1].plot(llrfref[0][idx1],llrfref[1][idx1],'k*')
            ax2.plot(np.full_like(epu[0],epu[0][idx2]/1000),epu[1],'k--')
            ax2.text(epu[0][idx2]/1000,epu[1][idx2]*0.8,'{:.2f}kW\n{:.2f}V\n{:.2f}ms'.format(llrfref[1][idx1],epu[1][idx2],llrfref[0][idx1]),**textkwargs)
    axs[1].set_xlabel('ms')
    axs[1].set_ylabel('kW',rotation=0,labelpad=20)
    ax2.set_ylabel('V',rotation=0,labelpad=20)
    lns=lns1+lns2
    axs[1].legend(lns,[l.get_label() for l in lns],**legendkwargs)
    canvas.fig.tight_layout()
    canvas.draw()

if __name__ == '__main__':
    pass

