#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#
import sys
import os
import math
import re
import copy
import h5py
import datetime
import numpy as np
import scipy.signal
from termcolor import colored
from p4p.client.thread import Context

import p4p
str_=(p4p.nt.enum.ntenum, p4p.nt.scalar.ntstr)

version={'maj':2,'min':2,'auth':'PP','when':'2022-07-12'}

logging.getLogger("p4p").setLevel(logging.CRITICAL)
sys.stderr = open(os.devnull, 'w')

def vstring():
    name=os.path.basename(sys.modules['__main__'].__file__).split(".")[0]
    return '{:s}, V.{:d}.{:d}, by {:s} on {:s}'.format(name,version['maj'],version['min'],version['auth'],version['when'])

# Cavity number
def GetCavityNumber(pva):
    selcav=pva.get('TS2-010CRM:EMR-RFPM-CAL:CableMode',timeout=10)
    print(selcav)
    return int(selcav)

# Prefixs initialization
def InitializePrefixes():
    SYSTEM='TS2-010RFC'
    CRM='TS2-010CRM'
    PICO='TS2-Row010'
    CAVITY={
            1:  {'sys' : SYSTEM,
                 'crm' : CRM, 
                 'dig' : '201',
                 'cav' : '010',
                 'chpu': '010', 
                 'chpf': '030', 
                 'chpr': '040',
                 'vpd' : '020',
                 'pico': PICO,
                 'pnum': '002',
                 'ppwr': 'B',
                 'pad1': 'C',
                 'pad2': 'D',
                 'pepu': 'G'},
            2:  {'sys' : SYSTEM, 
                 'crm' : CRM, 
                 'dig' : '202',
                 'cav' : '020',
                 'chpu': '020', 
                 'chpf': '030', 
                 'chpr': '040',
                 'vpd' : '020',
                 'pico': PICO,
                 'pnum': '002',
                 'ppwr': 'B',
                 'pad1': 'E',
                 'pad2': 'F',
                 'pepu': 'H'},
            3:  {'sys' : SYSTEM, 
                 'crm' : CRM, 
                 'dig' : '101',
                 'cav' : '030',
                 'chpu': '030', 
                 'chpf': '010', 
                 'chpr': '020',
                 'vpd' : '010',
                 'pico': PICO,
                 'pnum': '001',
                 'ppwr': 'B',
                 'pad1': 'C',
                 'pad2': 'D',
                 'pepu': 'G'},
            4:  {'sys' : SYSTEM, 
                 'crm' : CRM, 
                 'dig' : '102',
                 'cav' : '040',
                 'chpu': '040', 
                 'chpf': '010', 
                 'chpr': '020',
                 'vpd' : '010',
                 'pico': PICO,
                 'pnum': '001',
                 'ppwr': 'B',
                 'pad1': 'E',
                 'pad2': 'F',
                 'pepu': 'H'},
            }
    for k,v in CAVITY.items():
        v['Lpref']=v['sys']+':RFS-LLRF-'+v['dig']
        v['Dpref']=v['sys']+':RFS-DIG-'+v['dig']
        v['VPDpref']=v['sys']+':RFS-VPD-'+v['vpd']
        v['PZCpref']=v['sys']+':RFS-PZC-'+v['dig']
        v['Detunepref']=v['sys']+':EMR-CAV-'+v['cav']
        v['TUpref']=v['crm']+':EMR-CAV-'+v['cav']
        v['DCFpref']=v['crm']+':EMR-RFPM-'+v['chpf']
        v['DCRpref']=v['crm']+':EMR-RFPM-'+v['chpr']
        v['CAVpref']=v['crm']+':EMR-RFPM-'+v['chpu']
        v['HVpref']=v['crm']+':RFS-PS-'+v['cav']
        v['PICOpref']=v['pico']+':Ctrl-PScope-'+v['pnum']
        v['PPWRpref']=v['pico']+':Ctrl-PScope-'+v['pnum']+':'+v['ppwr']
        v['PAD1pref']=v['pico']+':Ctrl-PScope-'+v['pnum']+':'+v['pad1']
        v['PAD2pref']=v['pico']+':Ctrl-PScope-'+v['pnum']+':'+v['pad2']
        v['PEPUpref']=v['pico']+':Ctrl-PScope-'+v['pnum']+':'+v['pepu']

    return {'CAVS': CAVITY}

## LLRF Prefix
#def InitializeLLRFPrefixes():
#    SYSTEM='TS2-010RFC'
#    DIGITIZER={1: '201', 2: '202', 3: '101', 4: '102'}
#    CAVITY={1:  {'sys' : SYSTEM, 'dig' : DIGITIZER[1],
#                 'Lpref':SYSTEM+':RFS-LLRF-'+DIGITIZER[1],
#                 'Dpref':SYSTEM+':RFS-DIG-'+DIGITIZER[1]},
#            2:  {'sys' : SYSTEM, 'dig' : DIGITIZER[2],
#                 'Lpref':SYSTEM+':RFS-LLRF-'+DIGITIZER[2],
#                 'Dpref':SYSTEM+':RFS-DIG-'+DIGITIZER[2]},
#            3:  {'sys' : SYSTEM, 'dig' : DIGITIZER[3],
#                 'Lpref':SYSTEM+':RFS-LLRF-'+DIGITIZER[3],
#                 'Dpref':SYSTEM+':RFS-DIG-'+DIGITIZER[3]},
#            4:  {'sys' : SYSTEM, 'dig' : DIGITIZER[4],
#                 'Lpref':SYSTEM+':RFS-LLRF-'+DIGITIZER[4],
#                 'Dpref':SYSTEM+':RFS-DIG-'+DIGITIZER[4]}}
#    return {'CAVS': CAVITY}
#
## FFT Prefix
#def InitializeFFTPrefixes():
#    SYSTEM='TS2-010CRM'
#    CAV_NUMBER={1: '010', 2: '020', 3: '030', 4: '040'}
#    CAVITY={1:  {'sys' : SYSTEM, 'cav' : CAV_NUMBER[1],
#                  'TUpref' : SYSTEM+':EMR-CAV-'+CAV_NUMBER[1]},
#            2:  {'sys' : SYSTEM, 'cav' : CAV_NUMBER[2],
#                  'TUpref' : SYSTEM+':EMR-CAV-'+CAV_NUMBER[2]},
#            3:  {'sys' : SYSTEM, 'cav' : CAV_NUMBER[3],
#                  'TUpref' : SYSTEM+':EMR-CAV-'+CAV_NUMBER[3]},
#            4:  {'sys' : SYSTEM, 'cav' : CAV_NUMBER[4],
#                  'TUpref' : SYSTEM+':EMR-CAV-'+CAV_NUMBER[4]} }
#    return {'SYS': SYSTEM, 'CAVS': CAVITY}
#
## SRF Prefix
#def InitializeSRFPrefixes():
#    SYSTEM='TS2-010CRM'
#    CHCPU={1: '010', 2: '020', 3: '030', 4: '040'} # Cavity PU
#    CHCPF={1: '030', 2: '030', 3: '010', 4: '010'} # Directional coupler Close to the Cavity FWD
#    CHCPR={1: '040', 2: '040', 3: '020', 4: '020'} # Directional coupler Close to the Cavity REV
#    CAVITY={1: {'sys': SYSTEM, 'chpu': CHCPU[1], 'chpf': CHCPF[1], 'chpr': CHCPR[1],
#                'DCFpref':SYSTEM+':EMR-RFPM-'+CHCPF[1],
#                'DCRpref':SYSTEM+':EMR-RFPM-'+CHCPR[1],
#                'CAVpref':SYSTEM+':EMR-RFPM-'+CHCPU[1]},
#            2: {'sys': SYSTEM, 'chpu': CHCPU[2], 'chpf': CHCPF[2], 'chpr': CHCPR[2],
#                'DCFpref':SYSTEM+':EMR-RFPM-'+CHCPF[2],
#                'DCRpref':SYSTEM+':EMR-RFPM-'+CHCPR[2],
#                'CAVpref':SYSTEM+':EMR-RFPM-'+CHCPU[2]},
#            3: {'sys': SYSTEM, 'chpu': CHCPU[3], 'chpf': CHCPF[3], 'chpr': CHCPR[3],
#                'DCFpref':SYSTEM+':EMR-RFPM-'+CHCPF[3],
#                'DCRpref':SYSTEM+':EMR-RFPM-'+CHCPR[3],
#                'CAVpref':SYSTEM+':EMR-RFPM-'+CHCPU[3]},
#            4: {'sys': SYSTEM, 'chpu': CHCPU[4], 'chpf': CHCPF[4], 'chpr': CHCPR[4],
#                'DCFpref':SYSTEM+':EMR-RFPM-'+CHCPF[4],
#                'DCRpref':SYSTEM+':EMR-RFPM-'+CHCPR[4],
#                'CAVpref':SYSTEM+':EMR-RFPM-'+CHCPU[4]}}
#    return {'SYS': SYSTEM, 'CAVS': CAVITY}

# Timing
def ReportTiming(pva):
    dict_={
        'RF Pulse Width [us]'  : pva.get('TS2-010:Ctrl-EVM-101:RFWidth-RB'),
        'Repetition Rate [Hz]' : pva.get('TS2-010:Ctrl-EVM-101:Mxc-0-Frequency-RB')
    }
    val=[]
    print('* Timing Settings')
    for k,v in dict_.items():
        print('\t{:30s}:\t{:s}'.format(k,v.__repr__()))
        val.append(v)
    return {'LENGTH':val[0], 'FREQUENCY':val[1]}

# VPD
def ReportVPD(pva,CAV):
    position=pva.get(CAV['VPDpref']+':Motor.RBV')
    print('* VPD Settings\n\t{:30s}:\t{:s}'.format('Power Divider Position [mm]',position.__repr__()))
    return {'POS':position}

# SRF power sensors
def SRFPMOffset(pva,CAV_NUMBER,CAV):
    offset=[]
    label=['CAV{:d} PU'.format(CAV_NUMBER),'CAV{:d} FWD'.format(CAV_NUMBER),'CAV{:d} REF'.format(CAV_NUMBER)]
    chprefs=[CAV['CAVpref'],CAV['DCFpref'],CAV['DCRpref']]
    print('* Cavity {:d} SRF PM Configuration'.format(CAV_NUMBER))
    for l,ch in zip(label,chprefs):
        dic={
            '{:s} Cold Cable Enabled'.format(l)  : pva.get(ch+':ColdCableEn'),
            '{:s} Cold Cable Att [dB]'.format(l) : pva.get(ch+':ColdCableAtt'),
            '{:s} Warm Cable Att [dB]'.format(l) : pva.get(ch+':WarmOffset'),
            '{:s} Offset [dB]'.format(l)         : pva.get(ch+':Offset-RB')}
        offset.append(dic)
        for k,v in dic.items():
            if isinstance(v,str_): print('\t{:30s}:\t{:s}'.format(k,v.__str__()))
            else: print('\t{:30s}:\t{:s}'.format(k,v.__repr__()))
    kt=pva.get(CAV['CAVpref']+':EaccFactor')
    print('* Cavity {:d} SRF kt\n\t{:.3f}'.format(CAV_NUMBER,kt))
    return {'LABELS':label, 'OFFSETS':offset, 'kt':kt}

# HV bias
def ReportHVBias(pva,CAV_NUMBER,CAV):
    hv={
        'HV_SP [V]'    : pva.get(CAV['HVpref']+':Volt-SP'),
        'HV_RB [V]'    : pva.get(CAV['HVpref']+':Volt-RB'),
        'Curr_RB [mA]' : pva.get(CAV['HVpref']+':Current-RB'),
        'HV_Act'       : pva.get(CAV['HVpref']+':Volt-Act'),
        'HV_Status'    : pva.get(CAV['HVpref']+':HVStatus')
    }
    print('* Cavity {:d} HV Bias Settings'.format(CAV_NUMBER))
    for k,v in hv.items():
        if isinstance(v,str_): print('\t{:30s}:\t{:s}'.format(k,v.__str__()))
        else: print('\t{:30s}:\t{:s}'.format(k,v.__repr__()))
    return hv

# FF table parameters
def ReportFFTable(pva,CAV_NUMBER,CAV):
    # Modes from LLRF
    modes={'OpenLoop' : pva.get(CAV['Dpref']+':OpenLoop-RB'),
           'CnstFFEn' : pva.get(CAV['Dpref']+':RFCtrlCnstFFEn-RB'),
           'CnstSPEn' : pva.get(CAV['Dpref']+':RFCtrlCnstSPEn-RB')}
    # FF Table Parameters from LLRF
    ff={
        'FF_Enabled'         : pva.get(CAV['Lpref']+':FFPulseGenEn'),
        'FF_PowerP[kw]'      : pva.get(CAV['Lpref']+':FFPulseGenP'),
        'FF_FillRatio'       : pva.get(CAV['Lpref']+':FFPulseGenPfillRatio'),
        'FF_FillingTime[ms]' : pva.get(CAV['Lpref']+':FFPulseGenTfill'),
        'FF_RiseTime[ms]'    : pva.get(CAV['Lpref']+':FFPulseGenTslope'),
        'FF_Phase[deg]'      : pva.get(CAV['Lpref']+':FFPulseGenPhase')
    }
    sp={
        'SP_Enabled'         : pva.get(CAV['Lpref']+':SPRampingEn'),
        'SP_Field[MV/m]'     : pva.get(CAV['Lpref']+':SPRampingA'),
        'SP_Fill[ms]'        : pva.get(CAV['Lpref']+':SPRampingTfill'),
        'SP_Delay[ms]'       : pva.get(CAV['Lpref']+':SPRampingDelay'),
        'SP_Tau[ms]'         : pva.get(CAV['Lpref']+':SPRampingTao'),
        'SP_Phase[deg]'      : pva.get(CAV['Lpref']+':SPRampingPhase')
    }
    output_modes={'FF Pulse Generation':ff, 'SP Ramping':sp}
    for k,v in output_modes.items():
        print('* Cavity {:d} {:s} Settings'.format(CAV_NUMBER,k))
        for kk,vv in v.items():
            if isinstance(vv,str_): print('\t{:30s}:\t{:s}'.format(kk,vv.__str__()))
            else: print('\t{:30s}:\t{:s}'.format(kk,vv.__repr__()))
    print('* Retrieving FF and SP tables...',end='')
#    pva.put(CAV['Dpref']+":FFTbl-Mag-RB.PROC",1)    
#    pva.put(CAV['Dpref']+":FFTbl-Ang-RB.PROC",1)    
#    pva.put(CAV['Dpref']+":SPTbl-Mag-RB.PROC",1)    
#    pva.put(CAV['Dpref']+":SPTbl-Ang-RB.PROC",1)    
    tables={'spm':pva.get(CAV['Dpref']+":SPTbl-Mag-RB"),
            'spa':pva.get(CAV['Dpref']+":SPTbl-Ang-RB"),
            'ffm':pva.get(CAV['Dpref']+":FFTbl-Mag-RB"),
            'ffa':pva.get(CAV['Dpref']+":FFTbl-Ang-RB")}
    print('Done!')
    return {'PG':ff, 'SR':sp, 'TABLES':tables, 'MODES':modes}

# Piezo controller parameters
def ReportPiezoController(pva,CAV_NUMBER,CAV):
    list_=[]
    channel=['A','B']
    for ch in channel:
        list_.append({
            'Mode'           : pva.get(CAV['PZCpref']+':Ch{:s}Swit-SP'.format(ch)),
            'Source'         : pva.get(CAV['PZCpref']+':Ch{:s}WavPreTyp-SP'.format(ch)),
            'Wave type'      : pva.get(CAV['PZCpref']+':Ch{:s}WavTyp-SP'.format(ch)),
            'Amplitude [V]'  : pva.get(CAV['PZCpref']+':Ch{:s}WavAmp-RB'.format(ch)),
            'Frequency [Hz]' : pva.get(CAV['PZCpref']+':Ch{:s}WavFrq-RB'.format(ch)),
            'Offset [V]'     : pva.get(CAV['PZCpref']+':Ch{:s}WavOff-RB'.format(ch)),
            'Rise time [us]' : pva.get(CAV['PZCpref']+':Ch{:s}WavTrpTr-RB'.format(ch)),
            'Fill time [us]' : pva.get(CAV['PZCpref']+':Ch{:s}WavTrpTf-RB'.format(ch)),
            'Nmr of Samples' : pva.get(CAV['PZCpref']+':Ch{:s}WavLoadSampSug-RB'.format(ch)),
            'Trig Dly [s]'   : pva.get(CAV['PZCpref']+':Ch{:s}TrigDly-RB'.format(ch))
        })
    yesno=['OFF','ON']
    for i in range(0,len(list_)):
        print('* Cavity {:d} Piezo Controller \"{:s}\" Settings'.format(CAV_NUMBER,channel[i]))
        print('\tPiezo controller is \"{:s}\"'.format(yesno[list_[i]['Mode']]))
        for k,v in list_[i].items():
            if isinstance(v,str_): print('\t{:30s}:\t{:s}'.format(k,v.__str__()))
            else: print('\t{:30s}:\t{:s}'.format(k,v.__repr__()))
    return {'PZCchA':list_[0],'PZCchB':list_[1]}

# Cavity gradient calibration status
def GetCalStatus(pva,CAV_NUMBER,CAV):
    dict_={
        'Status'                   : pva.get(CAV['Dpref']+':AI0-CalGradEn'),
        'LLRF Kt [(MV/m)/sqrt(W)]' : pva.get(CAV['Dpref']+':AI0-CalGradKt'),
        'LLRF Att [dB]'            : pva.get(CAV['Dpref']+':AI0-CalGradA')
    }
    print('* Cavity {:d} Gradient calibration'.format(CAV_NUMBER),end='')
    if dict_['Status']: print(' is Enabled')
    else: print(' is Disabled')
    val=[]
    for k,v in dict_.items():
        val.append(v)
        if isinstance(v,str_): print('\t{:30s}:\t{:s}'.format(k,v.__str__()))
        else: print('\t{:30s}:\t{:s}'.format(k,v.__repr__()))
    return {'status':val[0], 'lkt':val[1], 'la':val[2]}     

# Picoscopes timing parameters
def ReportPicoTiming(pva,CAV_NUMBER,CAV):
    pico_timing={
        'Sampling rate [Hz]'     : pva.get(CAV['PICOpref']+':SamplingFreq'),
        'Samples before trigger' : pva.get(CAV['PICOpref']+':NumPreTrigSamples'),
        'Samples after trigger'  : pva.get(CAV['PICOpref']+':NumPostTrigSamples')
    }
    print('* Cavity {:d} Picoscope Timing Settings'.format(CAV_NUMBER))
    for k,v in pico_timing.items():
        print('\t{:30s}:\t{:s}'.format(k,v.__repr__()))
    return pico_timing

# Acquire trace
def AcquireTraces(pva,CAV_NUMBER,CAV,parameters): 
    status=parameters['RFCAL']['status']
    lkt=parameters['RFCAL']['lkt']
    la=parameters['RFCAL']['la']
    print('* Acquiring Traces...', end='')
    ########################################
    # # LLRF Channel readback
    # PVs for LLRF Cavity field Channel
    LLRFPTR=[CAV['Dpref']+':Dwn0-XAxis',
             CAV['Dpref']+':Dwn0-Cmp0',
             CAV['Dpref']+':Dwn0-Cmp1']
    # PVs for LLRF Klystron power Channel
    LLRFFWD=[CAV['Dpref']+':Dwn3-XAxis',
             CAV['Dpref']+':Dwn3-Cmp0',
             CAV['Dpref']+':Dwn3-Cmp1']
    # PVs for LLRF Cavity forward Channel
    LLRFCFW=[CAV['Dpref']+':Dwn5-XAxis',
             CAV['Dpref']+':Dwn5-Cmp0',
             CAV['Dpref']+':Dwn5-Cmp1']
    # PVs for LLRF Cavity reflected Channel
    LLRFCREF=[CAV['Dpref']+':Dwn6-XAxis',
             CAV['Dpref']+':Dwn6-Cmp0',
             CAV['Dpref']+':Dwn6-Cmp1']
    # LLRF Cavity field
    print(' LLRF',end='')
    llrfputrace=[pva.get(LLRFPTR[0]),pva.get(LLRFPTR[1]),pva.get(LLRFPTR[2])]
    llrfpu=copy.deepcopy(llrfputrace)
    llrfpuw=copy.deepcopy(llrfputrace)
    llrfpu_unit=[pva.get(LLRFPTR[0]+'.EGU'),pva.get(LLRFPTR[1]+'.EGU'),pva.get(LLRFPTR[2]+'.EGU')]
    llrfpuw_unit=[pva.get(LLRFPTR[0]+'.EGU'),pva.get(LLRFPTR[1]+'.EGU'),pva.get(LLRFPTR[2]+'.EGU')]
    if status:
        # status is True so trace in eacc and we need to compute power
        print(' (convert from Eacc to W),',end='')
        llrfpuw[1]=llrfputrace[1]*llrfputrace[1]/(lkt*lkt)
        llrfpuw_unit[1]='W'
    else:
        # status is False so trace in power and we need to calculate power at cavity, then field
        print(' (apply Att and convert to Eacc),',end='')
        llrfpuw[1]=llrfpu[1]*np.power(10,la/10)
        llrfpu[1]=lkt*np.sqrt(np.abs(llrfpuw[1]))
        llrfpu_unit[1]='MV/m'
    # LLRF Klystron power
    llrfkly=[pva.get(LLRFFWD[0]),pva.get(LLRFFWD[1]),pva.get(LLRFFWD[2])]
    llrfkly_unit=[pva.get(LLRFFWD[0]+'.EGU'),pva.get(LLRFFWD[1]+'.EGU'),pva.get(LLRFFWD[2]+'.EGU')]
    # LLRF Cavity forward
    llrffwd=[pva.get(LLRFCFW[0]),pva.get(LLRFCFW[1]),pva.get(LLRFCFW[2])]
    llrffwd_unit=[pva.get(LLRFCFW[0]+'.EGU'),pva.get(LLRFCFW[1]+'.EGU'),pva.get(LLRFCFW[2]+'.EGU')]
    # LLRF Cavity reflect
    llrfref=[pva.get(LLRFCREF[0]),pva.get(LLRFCREF[1]),pva.get(LLRFCREF[2])]    
    llrfref_unit=[pva.get(LLRFCREF[0]+'.EGU'),pva.get(LLRFCREF[1]+'.EGU'),pva.get(LLRFCREF[2]+'.EGU')]    
    ########################################
    # # SRF Channel readback
    # PVs for SRF Cavity forward Channel
    RFPMDCC=[CAV['DCFpref']+':TimeArray',
             CAV['DCFpref']+':WFPowerW']
    # PVs for SRF Cavity reflect Channel
    RFPMDCR=[CAV['DCRpref']+':TimeArray',
             CAV['DCRpref']+':WFPowerW']
    # PVs for SRF Cavity field (calibrated) Channel
    RFPMCAV=[CAV['CAVpref']+':TimeArray',
             CAV['CAVpref']+':WFPowerDBM']
    RFPMPUW=[CAV['CAVpref']+':TimeArray',
             CAV['CAVpref']+':WFPowerW']
    print(' SRF,',end='')
    # SRF Cavity forward
    cavfwd=[pva.get(RFPMDCC[0]),pva.get(RFPMDCC[1])/1000]
    cavfwd_unit=[pva.get(RFPMDCC[0]+'.EGU'),'kW']
    # SRF Cavity reflect
    cavref=[pva.get(RFPMDCR[0]),pva.get(RFPMDCR[1])/1000]
    cavref_unit=[pva.get(RFPMDCR[0]+'.EGU'),'kW']
    # SRF Cavity field [dBm]
    cavpu=[pva.get(RFPMCAV[0]),pva.get(RFPMCAV[1])]
    cavpu_unit=[pva.get(RFPMCAV[0]+'.EGU'),pva.get(RFPMCAV[1]+'.EGU')]
    cavpuw=[pva.get(RFPMPUW[0]),pva.get(RFPMPUW[1])]
    cavpuw_unit=[pva.get(RFPMPUW[0]+'.EGU'),pva.get(RFPMPUW[1]+'.EGU')]
    ########################################
    # PVs for "Tuner position" and "Cavity frequency"
    TUNER=[CAV['TUpref']+':Wa-Xarr',
           CAV['TUpref']+':Wa-Yarr']
    tuner=[pva.get(TUNER[0]),pva.get(TUNER[1])]
    print(' TUNER,',end='')
    ########################################
    # PVs for Detuning
    DETUNING=[CAV['Detunepref']+':WFDtgQlXAxis',
              CAV['Detunepref']+':WFCavDetune']
    detuning=[pva.get(DETUNING[0]),pva.get(DETUNING[1])]
    print(' Detuning,',end='')
    ########################################
    # PVs for Piezo Controller
    PZCAWAVPRE=[CAV['PZCpref']+':ChAWavArrX-RB',
                CAV['PZCpref']+':ChAWavArr-RB']
    PZCBWAVPRE=[CAV['PZCpref']+':ChBWavArrX-RB',
                CAV['PZCpref']+':ChBWavArr-RB']
    PZCAWAVCURR=[CAV['PZCpref']+':WavXChABCD-RB',
                 CAV['PZCpref']+':WavChA-RB']
    PZCAWAVVOLT=[CAV['PZCpref']+':WavXChABCD-RB',
                 CAV['PZCpref']+':WavChB-RB']
    PZCBWAVCURR=[CAV['PZCpref']+':WavXChABCD-RB',
                 CAV['PZCpref']+':WavChD-RB']
    PZCBWAVVOLT=[CAV['PZCpref']+':WavXChABCD-RB',
                 CAV['PZCpref']+':WavChC-RB']
    # pzc channel A waveform preview
    pzcAwavpre=[pva.get(PZCAWAVPRE[0]),pva.get(PZCAWAVPRE[1])]
    # pzc sensor channel A current waveform
    pzcAwavcurr=[pva.get(PZCAWAVCURR[0]),pva.get(PZCAWAVCURR[1])]
    # pzc sensor channel A voltage waveform
    pzcAwavvolt=[pva.get(PZCAWAVVOLT[0]),pva.get(PZCAWAVVOLT[1])]
     # pzc channel B waveform preview
    pzcBwavpre=[pva.get(PZCBWAVPRE[0]),pva.get(PZCBWAVPRE[1])]
    # pzc sensor channel B current waveform
    pzcBwavcurr=[pva.get(PZCBWAVCURR[0]),pva.get(PZCBWAVCURR[1])]
    # pzc sensor channel B voltage waveform
    pzcBwavvolt=[pva.get(PZCBWAVVOLT[0]),pva.get(PZCBWAVVOLT[1])]
    print(' Piezo Controller,',end='')
    ########################################
    # PVs for Picoscope
    PICOPWR=[CAV['PPWRpref']+'-TRC2ArrayTimeUs',
             CAV['PPWRpref']+'-TRC2ArrayData']
    PICOADAIR=[CAV['PAD1pref']+'-TRC2ArrayTimeUs',
               CAV['PAD1pref']+'-TRC2ArrayData']
    PICOADVAC=[CAV['PAD2pref']+'-TRC2ArrayTimeUs',
               CAV['PAD2pref']+'-TRC2ArrayData']
    PICOEPU=[CAV['PEPUpref']+'-TRC2ArrayTimeUs',
             CAV['PEPUpref']+'-TRC2ArrayData']
    # RF Power in mV
    picopwr=[pva.get(PICOPWR[0]),pva.get(PICOPWR[1])]
    # AD air
    picoadair=[pva.get(PICOADAIR[0]),pva.get(PICOADAIR[1])]
    # AD vac
    picoadvac=[pva.get(PICOADVAC[0]),pva.get(PICOADVAC[1])]
    # EPU
    picoepu=[pva.get(PICOEPU[0]),pva.get(PICOEPU[1])]
    print(' Picoscope,',end='')
    ########################################
    # # finish and return
    print(' Done!')
    return {'llrfpu':llrfpu,'llrfpu_unit':llrfpu_unit,'llrfpuw':llrfpuw,'llrfpuw_unit':llrfpuw_unit,
            'llrfkly':llrfkly,'llrfkly_unit':llrfkly_unit,'llrffwd':llrffwd,'llrffwd_unit':llrffwd_unit,'llrfref':llrfref,'llrfref_unit':llrfref_unit,
            'cavfwd':cavfwd,'cavfwd_unit':cavfwd_unit,'cavref':cavref,'cavref_unit':cavref_unit,
            'cavpu':cavpu,'cavpu_unit':cavpu_unit,'cavpuw':cavpuw,'cavpuw_unit':cavpuw_unit,
            'tuner':tuner,
            'detuning':detuning,
            'pzcAwavpre':pzcAwavpre, 'pzcAwavcurr':pzcAwavcurr, 'pzcAwavvolt':pzcAwavvolt, 'pzcBwavpre':pzcBwavpre, 'pzcBwavcurr':pzcBwavcurr, 'pzcBwavvolt':pzcBwavvolt,
            'picopwr':picopwr,'picoadair':picoadair,'picoadvac':picoadvac,'picoepu':picoepu
    }

# Saving trace into *.hdf5
def SaveTraces(pva,CM,CAV_NUMBER,CAV,tr,parameters): 
    # take the parameters
    kt=parameters['PM']['kt']
    offset=parameters['PM']['OFFSETS']
    timing=parameters['TIM']
    position=parameters['VPD']['POS']
    ff=parameters['FF']['PG']
    sp=parameters['FF']['SR']
    tables=parameters['FF']['TABLES']
    modes=parameters['FF']['MODES']
    hv=parameters['HV']
    pzc=parameters['PZC']
    rfcal=parameters['RFCAL']
    pico_timing=parameters['PICOTIM']
    # get LLRF data and units
    llrfpu=tr['llrfpu']
    llrfpu_unit=tr['llrfpu_unit']
    llrfpuw=tr['llrfpuw']
    llrfpuw_unit=tr['llrfpuw_unit']
    llrfkly=tr['llrfkly']
    llrfkly_unit=tr['llrfkly_unit']
    llrffwd=tr['llrffwd']
    llrffwd_unit=tr['llrffwd_unit']
    llrfref=tr['llrfref']
    llrfref_unit=tr['llrfref_unit']
    # get SRF data and units
    cavpu=tr['cavpu']
    cavpu_unit=tr['cavpu_unit']
    cavpuw=tr['cavpuw']
    cavpuw_unit=tr['cavpuw_unit']
    cavfwd=tr['cavfwd']
    cavfwd_unit=tr['cavfwd_unit']
    cavref=tr['cavref']
    cavref_unit=tr['cavref_unit']
    # get Tuner data
    tuner=tr['tuner']
    # get Detuning data
    detuning=tr['detuning']
    # get Piezo Controller data
    pzcAwavpre=tr['pzcAwavpre']
    pzcAwavcurr=tr['pzcAwavcurr']
    pzcAwavvolt=tr['pzcAwavvolt']
    pzcBwavpre=tr['pzcBwavpre']
    pzcBwavcurr=tr['pzcBwavcurr']
    pzcBwavvolt=tr['pzcBwavvolt']
    # get Picoscope data
    picopwr=tr['picopwr']
    picoadair=tr['picoadair']
    picoadvac=tr['picoadvac']
    picoepu=tr['picoepu']
    # Cavity gradient - SRF (averaging 20%~80%)
    pwr=cavpuw[1][(cavpuw[0]>0.2*timing['LENGTH']/1e6)&(cavpuw[0]<0.8*timing['LENGTH']/1e6)]
    avgpuw=np.average(pwr)
    eacc=np.sqrt(avgpuw)*kt
    print('* Acquired traces at (SRF) Eacc\n\t{:.3f}MV/m'.format(eacc))
    if tables['ffm'].size > 0:
        LastFFValue=tables['ffm'][-1]
    else:
        LastFFValue=ff['FF_PowerP[kw]']
    # Save data into *.hdf5
    filename='{:s}_CAV{:d}_{:s}_{:05.2f}MV_{:03.0f}kW.hdf5'.format(CM,CAV_NUMBER,datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S"),eacc,LastFFValue)
    saveDir = os.path.join(os.getcwd(),"{:s}".format(CM),"CAV{:d}".format(CAV_NUMBER)) 
    if not os.path.isdir(saveDir):
        os.makedirs(saveDir)
    savePath = os.path.join(saveDir,"{:s}".format(filename))
    with h5py.File(savePath,'x') as f:
        f.attrs["Datetime"]="{:s}".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        f.attrs["Location"]="TS2 LCR"
        f.attrs["Operator"]="SRF Team"
        f.attrs['VMAJ']=version['maj']
        f.attrs['VMIN']=version['min']

        f.attrs["CryoModule"]="{:s}".format(CM)
        f.attrs["Cavity"]="Cavity{:d}".format(CAV_NUMBER)
        f.attrs["Cavity Gradient[MV]"]=eacc
        f.attrs["RF Pulse Width[us]"]=timing['LENGTH']
        f.attrs["Repetition Rate[Hz]"]=timing['FREQUENCY']
        f.attrs["VPD Position[mm]"]=position
        f.attrs["kt"]=kt
        # with OSP230 RF switcher
        f.attrs["OSP230"]=1
        f.attrs['Version']=vstring()
        # FF table
        # for key,value in ff.items():
        #     f.attrs[key]=value
        # if tables['ffm'].size > 0:
        #     f.attrs['FF_keep']=tables['ffm'][-1]
        # else:
        #     f.attrs['FF_keep']=np.nan
        # if tables['spm'].size > 0:
        #     f.attrs['SP_keep']=tables['spm'][-1]
        # else:
        #     f.attrs['SP_keep']=np.nan
        
        #for key,value in ff.items():
            # if key == 'FF_Enabled':
            #     f.attrs[key]="{:d}".format(value)
            # else:
            #     f.attrs[key]="{:.3f}".format(value)
            #f.attrs[key]=value
        
        #grp=f.create_group('llrf')
        #llrf1=grp.create_dataset('LLRFPTR',shape=shape(llrfpu),data=llrfpu)
        #llrf1.attrs["Channel0"]="LLRF Cavity Field"

        # LLRF data group
        grp=f.create_group('llrf')
        grp.attrs["Note"]="Cavity data measurement by LLRF"
        for k,v in rfcal.items():
            grp.attrs[k]=v
        units_str_="{:s},{:s},{:s}"
        llrf1=grp.create_dataset('LLRFPTR',shape=np.shape(llrfpu),data=llrfpu)
        llrf1.attrs["AI0"]="LLRF Cavity Gradient"
        llrf1.attrs["Unit"]=units_str_.format(*llrfpu_unit)
        llrf1_1=grp.create_dataset('LLRFPTRW',shape=np.shape(llrfpuw),data=llrfpuw)
        llrf1_1.attrs["AI0"]="LLRF Cavity Power"
        llrf1_1.attrs["Unit"]=units_str_.format(*llrfpuw_unit)
        llrf2=grp.create_dataset('LLRFFWD',shape=np.shape(llrfkly),data=llrfkly)
        llrf2.attrs["AI3"]="LLRF Klystron Power"
        llrf2.attrs["Unit"]=units_str_.format(*llrfkly_unit)
        llrf3=grp.create_dataset('LLRFCFW',shape=np.shape(llrffwd),data=llrffwd)
        llrf3.attrs["AI5"]="LLRF Cavity Forward"
        llrf3.attrs["Unit"]=units_str_.format(*llrffwd_unit)
        llrf4=grp.create_dataset('LLRFCREF',shape=np.shape(llrfref),data=llrfref)
        llrf4.attrs["AI6"]="LLRF Cavity Reflect"
        llrf4.attrs["Unit"]=units_str_.format(*llrfref_unit)
        
        # SRF data group
        grp=f.create_group('srf')
        grp.attrs["Note"]="Cavity data measurement by SRF"
        units_str_="{:s},{:s}"
        srf1=grp.create_dataset('RFPMCAV',shape=np.shape(cavpu),data=cavpu)
        srf1.attrs["CHCPU"]="SRF Cavity PU"
        srf1.attrs["Unit"]=units_str_.format(*cavpu_unit)
        for key,value in offset[0].items():
            srf1.attrs[key]=value
        srf2=grp.create_dataset('RFPMPUW',shape=np.shape(cavpuw),data=cavpuw)
        srf2.attrs["CHCPU"]="SRF Cavity PU"
        srf2.attrs["Unit"]=units_str_.format(*cavpuw_unit)
        for key,value in offset[0].items():
            srf2.attrs[key]=value
        srf3=grp.create_dataset('RFPMDCC',shape=np.shape(cavfwd),data=cavfwd)
        srf3.attrs["CHCPF"]="SRF Cavity FWD at Cavity"
        srf3.attrs["Unit"]=units_str_.format(*cavfwd_unit)
        for key,value in offset[1].items():
            srf3.attrs[key]=value
        srf4=grp.create_dataset('RFPMDCR',shape=np.shape(cavref),data=cavref)
        srf4.attrs["CHCPR"]="SRF Cavity REF at Cavity"
        srf4.attrs["Unit"]=units_str_.format(*cavref_unit)
        for key,value in offset[2].items():
            srf4.attrs[key]=value

        # FFT data group
        grp=f.create_group('fft')
        grp.attrs["Note"]="Cavity frequency and tuner position measurement"
        dst=grp.create_dataset('TUNER',shape=np.shape(tuner),data=tuner)
        dst.attrs["xlabel"]="Number of full steps"
        dst.attrs["ylabel"]="Cavity frequency"
        dst.attrs["Unit"]="steps,kHz"

        # Detuning data group 
        grp=f.create_group('detuning')
        grp.attrs["Note"]="Detuning across the pulse"
        dst=grp.create_dataset('DETUNING',shape=np.shape(detuning),data=detuning)
        dst.attrs["xlabel"]="time"
        dst.attrs["ylabel"]="Detuning"
        dst.attrs["Unit"]="ms,Hz"

        # HV bias data group
        grp=f.create_group('hvbias')
        grp.attrs["Note"]="HV bias status"
        for k,v in hv.items():
            if isinstance(v,str_):
                grp.attrs[k]="{:s}".format(v.__str__())
            else:
                grp.attrs[k]=v

        # Tables data group
        grp=f.create_group('tables')
        grp.attrs['Note']="LLRF Tables and status"
        fs=pva.get(CAV['Lpref']+':FreqSampling')
        n=pva.get(CAV['Dpref']+':IQSmpNearIQ-N-RB')
        grp.attrs['dt']=1/(fs/n)
        for k,v in modes.items():
            grp.attrs[k]=v
        spm=grp.create_dataset('SPTableM',shape=np.shape(tables['spm']),data=tables['spm'])
        spa=grp.create_dataset('SPTableA',shape=np.shape(tables['spa']),data=tables['spa'])
        ffm=grp.create_dataset('FFTableM',shape=np.shape(tables['ffm']),data=tables['ffm'])
        ffa=grp.create_dataset('FFTableA',shape=np.shape(tables['ffa']),data=tables['ffa'])
        # FF/SP table
        for key,value in ff.items():
            grp.attrs[key]=value
        for key,value in sp.items():
            grp.attrs[key]=value
        if tables['ffm'].size > 0:
            grp.attrs['FF_keep']=tables['ffm'][-1]
        else:
            grp.attrs['FF_keep']=np.nan
        if tables['spm'].size > 0:
            grp.attrs['SP_keep']=tables['spm'][-1]
        else:
            grp.attrs['SP_keep']=np.nan

        # Piezo Controller data group
        grp=f.create_group('pzc')
        grp.attrs["Note"]="Piezo Controller channel A and B"
        channel=['A','B']
        pzc_wavpre=[pzcAwavpre,pzcBwavpre]
        pzc_wavcurr=[pzcAwavcurr,pzcBwavcurr]
        pzc_wavvolt=[pzcAwavvolt,pzcBwavvolt]
        # create datasets
        for i in range(0,len(channel)):
            # ChA and ChB waveform preview
            dst=grp.create_dataset('PZC{:s}WAVPRE'.format(channel[i]),shape=np.shape(pzc_wavpre[i]),data=pzc_wavpre[i])
            for k,v in pzc['PZCch{:s}'.format(channel[i])].items():
                if isinstance(v,str_):
                    dst.attrs[k]="{:s}".format(v.__str__())
                else:
                    dst.attrs[k]=v
            dst.attrs["Note"]="Channel {:s} waveform preview".format(channel[i])
            dst.attrs["xlabel"]="time"
            dst.attrs["ylabel"]="Voltage"
            dst.attrs["Unit"]="us,V"
            # ChA and ChB current output
            dst=grp.create_dataset('PZC{:s}WAVCURR'.format(channel[i]),shape=np.shape(pzc_wavcurr[i]),data=pzc_wavcurr[i])
            dst.attrs["Note"]="Channel {:s} current output waveform".format(channel[i])
            dst.attrs["xlabel"]="time"
            dst.attrs["ylabel"]="Current"
            dst.attrs["Unit"]="us,A"
            # ChA and ChB voltage output
            dst=grp.create_dataset('PZC{:s}WAVVOLT'.format(channel[i]),shape=np.shape(pzc_wavvolt[i]),data=pzc_wavvolt[i])
            dst.attrs["Note"]="Channel {:s} voltage output waveform".format(channel[i])
            dst.attrs["xlabel"]="time"
            dst.attrs["ylabel"]="Voltage"
            dst.attrs["Unit"]="us,V"
 
        # Picoscope data group
        grp=f.create_group('picoscope')
        grp.attrs["Note"]="Cavity activity signals measurement by Picoscope"
        for k,v in pico_timing.items():
            grp.attrs[k]=v
        pico1=grp.create_dataset('PICOPWR',shape=np.shape(picopwr),data=picopwr)
        pico1.attrs["Note"]="RF power waveform"
        pico1.attrs["Unit"]="us,mV"
        pico2=grp.create_dataset('PICOADAIR',shape=np.shape(picoadair),data=picoadair)
        pico2.attrs["Note"]="Arc detector signal waveform (air side)"
        pico2.attrs["Unit"]="us,V"
        pico3=grp.create_dataset('PICOADVAC',shape=np.shape(picoadvac),data=picoadvac)
        pico3.attrs["Note"]="Arc detector signal waveform (vacuum side)"
        pico3.attrs["Unit"]="us,V"
        pico4=grp.create_dataset('PICOEPU',shape=np.shape(picoepu),data=picoepu)
        pico4.attrs["Note"]="Electron pick-up signal waveform"
        pico4.attrs["Unit"]="us,V"

    f.close()
    return filename

if __name__ == '__main__':
    print('{:s}'.format(vstring()))
    ctxlist=Context.providers()
    print('Initializing EPICS {:s}'.format(*ctxlist))
    pva = Context('pva')
    # input cryomodule name
    CM=input('Input CM name: ').upper()
    if re.match("^CM\\d{2}$",CM):
        validname=True
    else:
        validname=False
    # get cavity number
    num=GetCavityNumber(pva)
    if num in [1,2,3,4]:
        validnum=True
    else:
        validnum=False
    if validname and validnum:
        CAV_NUMBER=num
        ##############################
        PVprefixes=InitializePrefixes()
        ##############################
        CAV=PVprefixes['CAVS'][CAV_NUMBER]
        ##############################
        print('**********************************{:s}**********************************'.format(CM))
        print('========================================================================')
        print('*** Selected Cavity {:d}, RFS system \"{:s}\" using digitizer \"{:s}\" ***'.format(CAV_NUMBER,CAV['sys'],CAV['dig']))
        print('========================================================================')
        # Initialize the parameters
        parameters={}
        # load Timing configuration
        parameters['TIM']=ReportTiming(pva)
        # load VPD status
        parameters['VPD']=ReportVPD(pva,CAV)
        # load SRF PM settings
        parameters['PM']=SRFPMOffset(pva,CAV_NUMBER,CAV)
        # load FF table
        parameters['FF']=ReportFFTable(pva,CAV_NUMBER,CAV)
        # load HV bias
        parameters['HV']=ReportHVBias(pva,CAV_NUMBER,CAV)
        # load Piezo controller configuration
        parameters['PZC']=ReportPiezoController(pva,CAV_NUMBER,CAV)
        # load llrf calibration
        parameters['RFCAL']=GetCalStatus(pva,CAV_NUMBER,CAV)
        # load Picoscope timing
        parameters['PICOTIM']=ReportPicoTiming(pva,CAV_NUMBER,CAV)
        # saving traces
        tr=AcquireTraces(pva,CAV_NUMBER,CAV,parameters)
        filename=SaveTraces(pva,CM,CAV_NUMBER,CAV,tr,parameters)
        print('* Data file\n\tData is stored in: {:s}/CAV{:d}/{:s}'.format(CM,CAV_NUMBER,filename))
    else:
        if not validname:
            print(colored("Oops! The parameter '{:s}' is not a valid CM name. Try again...".format(sys.argv[1]),"red"))
        if not validnum:
            print(colored("Oops! The parameter '{:d}' is not a valid cavity number between 1 and 4. Try again...".format(num),"red"))
