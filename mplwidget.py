#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Imports
from PyQt5 import QtWidgets
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas, NavigationToolbar2QT as NavigationToolbar
import matplotlib
import matplotlib.pyplot as plt

# Ensure using PyQt5 backend
matplotlib.use('QT5Agg')

# style sheet reference
matplotlib.pyplot.style.use("ggplot")

# Matplotlib canvas class to create figure
class MplCanvas(FigureCanvas):
    def __init__(self, parent=None, width=10, height=8, dpi=100):
        self.fig = Figure(figsize=(width,height), dpi=dpi)
        #self.ax = self.fig.add_subplot(111)
        FigureCanvas.__init__(self,self.fig)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(self,
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding
        )
        FigureCanvas.updateGeometry(self)

# Matplotlib widget
class MplWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self,parent)	# Inherit from QWidget
        self.canvas = MplCanvas()		# Create canvas object
        self.vbl = QtWidgets.QVBoxLayout()	# Set box for plotting
        self.vbl.addWidget(NavigationToolbar(self.canvas))
        self.vbl.addWidget(self.canvas)
        self.setLayout(self.vbl)
